<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\File
 *
 * @property int $id
 * @property string $name Название файла
 * @property string $folder_id К какой папке прикреплен
 * @property string $size Размер файла
 * @property int $user_id Владелец
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|File newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|File newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|File query()
 * @method static \Illuminate\Database\Eloquent\Builder|File whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|File whereFolderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|File whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|File whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|File whereSize($value)
 * @method static \Illuminate\Database\Eloquent\Builder|File whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|File whereUserId($value)
 * @mixin \Eloquent
 * @property string $link Уникальная ссылка
 * @property string $time_life До какого числа хранится
 * @method static \Illuminate\Database\Eloquent\Builder|File whereLink($value)
 * @method static \Illuminate\Database\Eloquent\Builder|File whereTimeLife($value)
 * @property string $path Путь до файла
 * @method static \Illuminate\Database\Eloquent\Builder|File wherePath($value)
 */
class File extends Model
{
    use HasFactory;

    const ATTR_ID = 'id';
    const ATTR_NAME = 'name';
    const ATTR_PATH = 'path';
    const ATTR_SIZE = 'size';

    protected $fillable = [
        'id',
        'name',
        'size',
        'link',
        'time_life'
    ];
}
