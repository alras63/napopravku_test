<?php

namespace App\Console;

use App\Models\File;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Storage;

class Kernel extends ConsoleKernel
{
    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->call(function () {
            $filesToDelete = File::where('time_life', '<=', Carbon::now('MSK'))->get();

            foreach ($filesToDelete as $fileModel) {
                $path = str_replace('/var/www/html/storage/app', '',  $fileModel->path);
                Storage::delete($path . '/' . $fileModel->name);
                $fileModel->delete();
            }

            return true;
        })->everyFiveMinutes();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
