<?php

namespace App\Helpers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class DirectoryHelper
{
    const MAX_USER_DIRECTORY_SIZE = 104857600;

    /**
     * Функция проверяет возможность загрузки в директорию по размеру
     *
     * @param float $size in kb
     * @return boolean
     * @author Рассохин Алексей <telegram: @alras63>
     */
    public static function canUploadBySize(float $size): bool
    {
        $directories = Storage::disk('local')->directories(Auth::id());

        $sizeMoment = 0;

        foreach ($directories as $dir) {
            $files = Storage::disk('local')->files($dir);

            foreach ($files as $key => $file) {
                $sizeMoment += Storage::disk('local')->size($file);
            }

        }

        if (($sizeMoment + $size) >= static::MAX_USER_DIRECTORY_SIZE) {
            return false;
        }

        return true;
    }

    /**
     * Функция получает размер всех файлов в директории
     *
     * @param string $path
     * @return float
     * @author Рассохин Алексей <telegram: @alras63>
     */
    public static function directorySize(string $path): float
    {
        $sizeMoment = 0;

        $directories = Storage::disk('local')->directories($path);

        foreach ($directories as $dir) {
            $sizeMoment += static::directorySize($dir);
        }

        $files = Storage::disk('local')->files($path);

        foreach ($files as $key => $file) {
            $sizeMoment += Storage::disk('local')->size($file);
        }

        return $sizeMoment;
    }

    /**
     * Format bytes to kb, mb, gb, tb
     *
     * @param float $size
     * @param integer $precision
     * @return float
     */
    public static function formatBytes(float $size, int $precision = 2)
    {
        if ($size > 0) {
            $size     = (int)$size;
            $base     = log($size) / log(1024);
            $suffixes = array(' bytes',
                              ' KB',
                              ' MB',
                              ' GB',
                              ' TB');

            return round(pow(1024, $base - floor($base)), $precision) . $suffixes[floor($base)];
        } else {
            return $size;
        }
    }
}
