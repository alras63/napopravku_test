<?php

namespace App\Http\Controllers\Api;

use App\Helpers\DirectoryHelper;
use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use \Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpFoundation\Response as ResponseAlias;
use Throwable;

class FoldersController extends Controller
{
    /**
     * Создание нового файла
     *
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     * @throws Throwable
     * @author Рассохин Алексей <telegram: @alras63>
     */
    public function create(Request $request): JsonResponse
    {
        $validate = Validator::make($request->all(), [
            'name' => ['required',
                       'string'
            ]
        ]);

        if ($validate->fails()) {
            if ($validate->fails()) {
                return Response::json(['errors' => $validate->errors()],
                    ResponseAlias::HTTP_BAD_REQUEST);
            }
        }

        $validate = $validate->validated();

        $isIsset = Storage::disk('local')->directoryExists(Auth::id() . '/' . $validate['name']);

        if ($isIsset) {
            return Response::json(['errors' => 'Папка с таким именем уже существует'],
                ResponseAlias::HTTP_BAD_REQUEST);
        }

        $storageDir = Storage::disk('local')->makeDirectory(Auth::id() . '/' . $validate['name']);

        if ($storageDir) {
            return Response::json(
                ['data' => $storageDir],
                ResponseAlias::HTTP_ACCEPTED
            );
        } else {
            return Response::json(['errors' => 'Произошла ошибка при выполнении действия "Создание папки"'],
                ResponseAlias::HTTP_SERVICE_UNAVAILABLE);
        }
    }

    /**
     * Просмотр содержимого папки
     *
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     * @throws Throwable
     * @author Рассохин Алексей <telegram: @alras63>
     */
    public function show(Request $request): JsonResponse
    {
        $validate = Validator::make($request->all(), [
            'name' => ['required',
                                  'string']
        ]);

        if ($validate->fails()) {
            if ($validate->fails()) {
                return Response::json(['errors' => $validate->errors()],
                    ResponseAlias::HTTP_BAD_REQUEST);
            }
        }

        $validate = $validate->validated();

        $folder = Storage::disk('local')->files(Auth::id() . '/' . $validate['name']);

        return Response::json(
            ['data' => $folder],
            ResponseAlias::HTTP_ACCEPTED
        );
    }

    /**
     * Получение списка своих папок
     *
     * @return JsonResponse
     * @author Рассохин Алексей <telegram: @alras63>
     */
    public function list(): JsonResponse
    {
        $folders = Storage::disk('local')->directories(Auth::id());

        if ($folders) {
            $result = [];
            foreach ($folders as $key => $folder) {
                $result[$key] = ['folder' => $folder,
                                 'size'   => DirectoryHelper::formatBytes(DirectoryHelper::directorySize($folder))];
            }
            return Response::json(
                ['data' => $result],
                ResponseAlias::HTTP_ACCEPTED
            );
        }


        return Response::json(['errors' => 'Произошла ошибка'],
            ResponseAlias::HTTP_SERVICE_UNAVAILABLE);
    }

}
