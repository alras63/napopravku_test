<?php

namespace App\Http\Controllers\Api;

use App\Helpers\DirectoryHelper;
use App\Http\Controllers\Controller;
use App\Models\File;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpFoundation\Response as ResponseAlias;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Throwable;

class DiskController extends Controller
{
    /**
     * Получение размера папки
     *
     * @param Request $request
     * @return float|JsonResponse
     * @throws ValidationException
     * @throws Throwable
     * @author Рассохин Алексей <telegram: @alras63>
     */
    public function showFolderSize(Request $request): float|JsonResponse
    {
        $validate = Validator::make($request->all(), [
            'folder_name' => ['required',
                              'string'],
        ]);

        if ($validate->fails()) {
            if ($validate->fails()) {
                return Response::json(['errors' => $validate->errors()],
                    ResponseAlias::HTTP_BAD_REQUEST);
            }
        }

        $validate = $validate->validated();

        $path = Auth::id() . '/' . $validate['folder_name'];

        $exists = Storage::disk('local')->directoryExists($path);

        if(!$exists) {
            return Response::json(['errors' => 'Такой папки не существует'],
                ResponseAlias::HTTP_BAD_REQUEST);
        }

        return Response::json(['data' =>  DirectoryHelper::formatBytes(DirectoryHelper::directorySize($path))],
            ResponseAlias::HTTP_ACCEPTED);
    }

    /**
     * Получение размера диска
     *
     * @return float|JsonResponse
     * @throws Throwable
     * @author Рассохин Алексей <telegram: @alras63>
     */
    public function showDiskSize(): float|JsonResponse
    {
        $path = Auth::id();

        $exists = Storage::disk('local')->directoryExists($path);

        if(!$exists) {
            return Response::json(['errors' => 'Произошла ошибка'],
                ResponseAlias::HTTP_SERVICE_UNAVAILABLE);
        }

        return Response::json(['data' =>  DirectoryHelper::formatBytes(DirectoryHelper::directorySize($path))],
            ResponseAlias::HTTP_ACCEPTED);
    }

    /**
     * Получение публичной ссылки на файл
     *
     * @param Request $request
     * @return string
     * @throws ValidationException
     * @author Рассохин Алексей <telegram: @alras63>
     */
    public function getPublicLink(Request $request): string
    {
        $validate = Validator::make($request->all(), [
            'folder_name' => ['required',
                              'string'],
            'file_name' => ['required',
                              'string'],
        ]);

        if ($validate->fails()) {
            if ($validate->fails()) {
                return Response::json(['errors' => $validate->errors()],
                    ResponseAlias::HTTP_BAD_REQUEST);
            }
        }

        $validate = $validate->validated();

        $path = storage_path('app') . "/" . Auth::id() . '/' . $validate['folder_name'];

        $fileModel = File::where(File::ATTR_PATH, '=', $path)->where(File::ATTR_NAME, '=', $validate['file_name'])->first();

        if(null !== $fileModel) {
            return env("APP_URL") . "/disk/get/file/" . $fileModel->link;
        }

        return "";
    }

    /**
     * Получение файла
     *
     * @throws Throwable
     * @author Рассохин Алексей <telegram: @alras63>
     */
    public function getFile(string $file_id): StreamedResponse|null
    {
        $fileModel = File::whereLink($file_id)->first();

        if(null !== $fileModel) {
            $path = str_replace('/var/www/html/storage/app', '',  $fileModel->path);
            return Storage::response($path . '/' . $fileModel->name);
        }

        return null;
    }
}
