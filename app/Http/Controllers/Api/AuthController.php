<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use \Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpFoundation\Response as ResponseAlias;

class AuthController extends Controller
{
    /**
     * Создает нового пользователя в системе
     *
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     * @author Рассохин Алексей <telegram: @alras63>
     */
    public function register(Request $request): JsonResponse
    {
        $validate = Validator::make($request->all(), [
            User::ATTR_NAME     => ['required',
                                    'string'],
            User::ATTR_EMAIL    => ['required',
                                    'string',
                                    'unique:users'],
            User::ATTR_LOGIN    => ['required',
                                    'string',
                                    'unique:users'],
            User::ATTR_PASSWORD => ['required',
                                    'string'],
        ]);

        if ($validate->fails()) {
            if ($validate->fails()) {
                return Response::json(['errors' => $validate->errors()],
                    ResponseAlias::HTTP_BAD_REQUEST);
            }
        }

        $validate = $validate->validated();

        $user = new User();
        $user->fill($validate);
        $user->password = Hash::make($validate['password']);

        if ($user->save()) {
            return Response::json(
                ['data' => 'Успешная регистрация пользователя'],
                ResponseAlias::HTTP_ACCEPTED
            );
        }

        return Response::json(['errors' => 'Произошла ошибка при выполнении действия "Регистрация"'],
            ResponseAlias::HTTP_SERVICE_UNAVAILABLE);
    }

    /**
     * Авторизация пользователя в системе
     *
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     * @author Рассохин Алексей <telegram: @alras63>
     */
    public function login(Request $request): JsonResponse
    {
        $validate = Validator::make($request->all(), [
            User::ATTR_EMAIL    => ['required_without:login',
                                    'string'],
            User::ATTR_LOGIN    => ['required_without:email',
                                    'string'],
            User::ATTR_PASSWORD => ['required',
                                    'string'],
        ]);

        if ($validate->fails()) {
            if ($validate->fails()) {
                return Response::json(['errors' => $validate->errors()],
                    ResponseAlias::HTTP_BAD_REQUEST);
            }
        }

        $validate = $validate->validated();

        $isAuth = Auth::attempt($validate);

        if ($isAuth) {
            $user            = Auth::user();
            $token           = $user->createToken('Bearer')->plainTextToken;
            $user->api_token = $token;
            $user->save([User::ATTR_API_TOKEN]);

            return Response::json(
                ['data' => [User::ATTR_API_TOKEN => $token]],
                ResponseAlias::HTTP_ACCEPTED
            );
        }

        return Response::json(['errors' => 'Произошла ошибка при выполнении действия "Авторизация"'],
            ResponseAlias::HTTP_FORBIDDEN);
    }

    /**
     * Получение краткой информации о пользователе
     *
     * @return JsonResponse
     * @author Рассохин Алексей <telegram: @alras63>
     */
    public function user(): JsonResponse
    {
        if (Auth::check()) {
            return Response::json(
                Auth::user(),
                ResponseAlias::HTTP_ACCEPTED
            );
        }

        return Response::json(['errors' => 'У вас нет прав на выполнение данного действия'],
            ResponseAlias::HTTP_FORBIDDEN);
    }
}
