<?php

namespace App\Http\Controllers\Api;

use App\Helpers\DirectoryHelper;
use App\Http\Controllers\Controller;
use App\Models\File;
use App\Models\Folder;
use App\Rules\NoConvertFileExtension;
use App\Rules\WithFileExtension;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use \Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpFoundation\Response as ResponseAlias;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Throwable;

class FilesController extends Controller
{
    /**
     * Создание нового файла
     *
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     * @throws Throwable
     * @author Рассохин Алексей <telegram: @alras63>
     */
    public function create(Request $request): JsonResponse
    {
        $validate = Validator::make($request->all(), [
            'file'        => ['required',
                              'file',
                              'max:20480'],
            'folder_name' => ['required',
                              'string'],
            'time_life'   => ['date']
        ]);

        if ($validate->fails()) {
            if ($validate->fails()) {
                return Response::json(['errors' => $validate->errors()],
                    ResponseAlias::HTTP_BAD_REQUEST);
            }
        }

        $validate = $validate->validated();

        $file = $validate['file'];

        if ($file->getMimeType() !== 'text/x-php') {
            if (!DirectoryHelper::canUploadBySize($file->getSize())) {
                return Response::json(['errors' => 'Ваше хранилище переполнено'],
                    ResponseAlias::HTTP_BAD_REQUEST);
            }

            if (Storage::disk('local')->fileExists(Auth::id() . '/' . $validate['folder_name'] . '/' . $file->getClientOriginalName())) {
                return Response::json(['errors' => 'Файл с таким названием уже существует'],
                    ResponseAlias::HTTP_BAD_REQUEST);
            }

            $size = $file->getSize();
            $name = $file->getClientOriginalName();

            $file = Storage::disk('local')->putFileAs(Auth::id() . '/' . $validate['folder_name'], $file, $file->getClientOriginalName());

            if ($file) {
                $fileModel = new File();
                $fileModel->name = $name;
                $fileModel->path = storage_path('app') . '/' . Auth::id() . '/' . $validate['folder_name'];
                $fileModel->size = $size;
                $fileModel->link = Str::random();
                $fileModel->time_life = $validate['time_life'] ?? NULL;

                if($fileModel->save()) {
                    return Response::json(
                        ['data' => $file],
                        ResponseAlias::HTTP_ACCEPTED
                    );
                }
            }
        } else {
            return Response::json(['errors' => 'Нельзя загружать php файлы'],
                ResponseAlias::HTTP_BAD_REQUEST);
        }

        return Response::json(['errors' => 'Произошла ошибка при выполнении действия "Создание файла"'],
            ResponseAlias::HTTP_SERVICE_UNAVAILABLE);

    }

    /**
     * Переименовать файл
     *
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     * @throws Throwable
     * @author Рассохин Алексей <telegram: @alras63>
     */
    public function rename(Request $request): JsonResponse
    {
        $old_file_name = $request->input('old_file_name');
        $ext           = null;
        preg_match("/^(.+)*\.(.+)$/i", $old_file_name, $ext);

        if (null !== $ext && isset($ext[2])) {
            $validate = Validator::make($request->all(), [
                'old_file_name' => ['required',
                                    new WithFileExtension()],
                'new_file_name' => ['required',
                                    'string',
                                    "regex:/(.+)*\.($ext[2])$/i"],
                'folder_name'   => ['required',
                                    'string'],
            ]);

            if ($validate->fails()) {
                if ($validate->fails()) {
                    return Response::json(['errors' => $validate->errors()],
                        ResponseAlias::HTTP_BAD_REQUEST);
                }
            }

            $validate = $validate->validated();

            if (!Storage::disk('local')->fileExists(Auth::id() . '/' . $validate['folder_name'] . '/' . $validate['old_file_name'])) {
                return Response::json(['errors' => 'Файла с таким названием нет'],
                    ResponseAlias::HTTP_BAD_REQUEST);
            }

            $path = storage_path('app') . "/" . Auth::id() . '/' . $validate['folder_name'];

            $fileModel = File::where(File::ATTR_PATH, '=', $path)->where(File::ATTR_NAME, '=', $validate['old_file_name'])->first();
            $fileModel->name = $validate['new_file_name'];
            $fileModel->save();

            $newFileName = Storage::disk('local')->move(Auth::id() . '/' . $validate['folder_name'] . '/' . $validate['old_file_name'], Auth::id() . '/' . $validate['folder_name'] . '/' . $validate['new_file_name']);

            return Response::json(
                ['data' => $newFileName],
                ResponseAlias::HTTP_ACCEPTED
            );
        }

        return Response::json(['errors' => 'Некорректные входные параметры'],
            ResponseAlias::HTTP_BAD_REQUEST);
    }

    /**
     * Удалить файл
     *
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     * @throws Throwable
     * @author Рассохин Алексей <telegram: @alras63>
     */
    public function delete(Request $request): JsonResponse
    {

        $validate = Validator::make($request->all(), [
            'file_name'   => ['required',
                              new WithFileExtension()],
            'folder_name' => ['required',
                              'string'],
        ]);

        if ($validate->fails()) {
            if ($validate->fails()) {
                return Response::json(['errors' => $validate->errors()],
                    ResponseAlias::HTTP_BAD_REQUEST);
            }
        }

        $validate = $validate->validated();

        if (!Storage::disk('local')->fileExists(Auth::id() . '/' . $validate['folder_name'] . '/' . $validate['file_name'])) {
            return Response::json(['errors' => 'Файла с таким названием нет'],
                ResponseAlias::HTTP_BAD_REQUEST);
        }

        $deleted = Storage::disk('local')->delete(Auth::id() . '/' . $validate['folder_name'] . '/' . $validate['file_name']);

        return Response::json(
            ['data' => $deleted],
            ResponseAlias::HTTP_ACCEPTED
        );
    }

    /**
     * Скачать файл
     *
     * @param Request $request
     * @return StreamedResponse|JsonResponse
     * @throws ValidationException
     * @throws Throwable
     * @author Рассохин Алексей <telegram: @alras63>
     */
    public function download(Request $request): StreamedResponse|JsonResponse
    {
        $validate = Validator::make($request->all(), [
            'file_name'   => ['required',
                              new WithFileExtension()],
            'folder_name' => ['required',
                              'string'],
        ]);

        if ($validate->fails()) {
            if ($validate->fails()) {
                return Response::json(['errors' => $validate->errors()],
                    ResponseAlias::HTTP_BAD_REQUEST);
            }
        }

        $validate = $validate->validated();

        if (!Storage::disk('local')->fileExists(Auth::id() . '/' . $validate['folder_name'] . '/' . $validate['file_name'])) {
            return Response::json(['errors' => 'Файла с таким названием нет'],
                ResponseAlias::HTTP_BAD_REQUEST);
        }

        return Storage::disk('local')->download(Auth::id() . '/' . $validate['folder_name'] . '/' . $validate['file_name']);
    }
}
