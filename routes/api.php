<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/login', function (Request $request) {
    return (new \App\Http\Controllers\Api\AuthController())->login($request);
});

Route::post('/register', function (Request $request) {
    return (new \App\Http\Controllers\Api\AuthController())->register($request);
});



Route::middleware('bearer:api')->group(function () {
    Route::get('/user', function () {
        return (new \App\Http\Controllers\Api\AuthController())->user();
    });

    Route::post('/folders/create', function (Request $request) {
        return (new \App\Http\Controllers\Api\FoldersController())->create($request);
    });
    Route::get('/folders/list', function () {
        return (new \App\Http\Controllers\Api\FoldersController())->list();
    });

    Route::get('/folders/show', function (Request $request) {
        return (new \App\Http\Controllers\Api\FoldersController())->show($request);
    });

    Route::post('/files/create', function (Request $request) {
        return (new \App\Http\Controllers\Api\FilesController())->create($request);
    });

    Route::post('/files/rename', function (Request $request) {
        return (new \App\Http\Controllers\Api\FilesController())->rename($request);
    });

    Route::post('/files/delete', function (Request $request) {
        return (new \App\Http\Controllers\Api\FilesController())->delete($request);
    });

    Route::post('/disk/showFolderSize', function (Request $request) {
        return (new \App\Http\Controllers\Api\DiskController())->showFolderSize($request);
    });

    Route::post('/disk/showDiskSize', function () {
        return (new \App\Http\Controllers\Api\DiskController())->showDiskSize();
    });

    Route::post('/disk/getPublicLink', function (Request $request) {
        return (new \App\Http\Controllers\Api\DiskController())->getPublicLink($request);
    });

    Route::get('/files/download', function (Request $request) {
        return (new \App\Http\Controllers\Api\FilesController())->download($request);
    });
});

//Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//    return $request->user();
//});
